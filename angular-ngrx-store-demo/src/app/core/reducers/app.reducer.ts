import {isDevMode} from '@angular/core';
import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import {counterReducer, CountState} from "./counter.reducer";
import {postReducer, PostsState} from "./post.reducer";

export interface IAppState {
  counter: CountState
}

export const appReducer: ActionReducerMap<IAppState> = {
  counter:counterReducer,
};


export const metaReducers: MetaReducer<IAppState>[] = isDevMode() ? [] : [];
