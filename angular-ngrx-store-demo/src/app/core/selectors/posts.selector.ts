import {createSelector} from "@ngrx/store";
import {IAppState} from "../reducers/app.reducer";

export const selectPostState = (state: IAppState) => state.posts
export const selectPosts= createSelector(selectPostState,state => state.post);
