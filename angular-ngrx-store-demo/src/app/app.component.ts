import {Component, OnInit} from '@angular/core';
import {count, map, Observable, pipe} from "rxjs";
import {select, Store} from "@ngrx/store";
import {CountState} from "./core/reducers/counter.reducer";
import {decrement, increment, reset} from "./core/actions/counter.action";
import {selectCount} from "./core/selectors/counter.selector";
import {IAppState} from "./core/reducers/app.reducer";


export interface Person {
  name?:string
  age?:number
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {

  count$: Observable<number> | undefined;

  files:any[] = [];

  constructor(private store: Store<IAppState>) {

      }

  ngOnInit(): void {
    /*this.count$ = this.store.pipe(
      select(state =>  state['counter'].count)
    )*/

   /* this.count$ = this.store.select(selectCount);*/

  }

 /* handleIncrement() {
    this.store.dispatch(increment());
  }

  handleDecrement() {
    this.store.dispatch(decrement());
  }

  handleReset() {
    this.store.dispatch(reset());
  }*/

  /*fileBrowseHandler(event:any) {
    console.log(event.target.files);
    for (const item of event.target.files) {
      const reader = new FileReader();
      const blob = item.slice(0, 4);
      reader.readAsArrayBuffer(blob);


      reader.addEventListener('load',() => {
        this.files.push(item);
      })
    }
  }*/

  /*formatBytes(bytes: number, decimals = 2) {
    if (bytes === 0) {
      return "0 Bytes";
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }*/
}
