import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {CountState} from "../../core/reducers/counter.reducer";
import {addCountValue} from "../../core/actions/counter.action";
import {IAppState} from "../../core/reducers/app.reducer";
import {selectCount} from "../../core/selectors/counter.selector";
import {Observable} from "rxjs";

@Component({
  selector: 'app-add-counter',
  templateUrl: './add-counter.component.html',
  styleUrl: './add-counter.component.scss'
})
export class AddCounterComponent implements OnInit {
  countVal: any;
  count$: Observable<number> | undefined;

  constructor(private store:Store<IAppState>) {
  }

  ngOnInit(): void {
    this.count$=this.store.select(selectCount);
  }


  handleAddCount() {
    this.store.dispatch(addCountValue({value:this.countVal}))
  }
}
