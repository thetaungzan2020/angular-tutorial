import {CountState} from "./counter.reducer";
import {createReducer, on} from "@ngrx/store";

export interface Post {
  id:number,
  title:string,
  description:string
}

export interface PostsState {
  post: Post[];
}

export const initialPostState: PostsState = {
  post: [
    {id:1,title:'What is Lorem Ipsum?',description:'"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."\n' +
        '"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."'},
    {id:2,title:'Why do we use it?',description:'"Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.'},
    {id:3,title:'Where does it come from?',description:'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.'},
    {id:4,title:'What is Lorem Ipsum?',description:'"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."\n' +
        '"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."'},
    {id:5,title:'Where does it come from?',description:'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.'},
    {id:6,title:'Why do we use it?',description:'"Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.'},

  ]
}

export const postReducer = createReducer(
  initialPostState,
)
