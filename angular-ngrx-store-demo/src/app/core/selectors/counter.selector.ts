import {CountState} from "../reducers/counter.reducer";
import {createSelector} from "@ngrx/store";
import {IAppState} from "../reducers/app.reducer";

export const selectCountState = (state: IAppState) => state.counter;

export const selectCount = createSelector(selectCountState,(state) => state.count);
