import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NonpersonComponent } from './nonperson.component';

describe('NonpersonComponent', () => {
  let component: NonpersonComponent;
  let fixture: ComponentFixture<NonpersonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NonpersonComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NonpersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
