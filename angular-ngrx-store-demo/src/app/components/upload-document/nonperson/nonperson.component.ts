import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-nonperson',
  templateUrl: './nonperson.component.html',
  styleUrl: './nonperson.component.scss'
})
export class NonpersonComponent {
  @Input() title = '';
}
