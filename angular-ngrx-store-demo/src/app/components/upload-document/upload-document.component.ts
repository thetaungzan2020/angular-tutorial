import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-upload-document',
  templateUrl: './upload-document.component.html',
  styleUrl: './upload-document.component.scss'
})
export class UploadDocumentComponent implements OnInit,AfterViewInit,OnDestroy {
  nonePersonList: any[] = [];

  constructor() {
  }

  ngOnInit(): void {

    this.nonePersonList = [
      "National Registration Identity Card of All Directors and Shareholders",
      "Signed Board Resolution",
      "Memorandum and Articles of Association (M&AA) or Constitution"
    ]
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    console.log(' ========= Bye Upload document ========= ');
  }



}
