import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AddCounterComponent} from "./components/add-counter/add-counter.component";
import {HomeComponent} from "./components/home/home.component";
import {UploadDocumentComponent} from "./components/upload-document/upload-document.component";

const routes: Routes = [
  {
    path:'',
    component:HomeComponent
  },
  {
    path: 'counter',
    component: AddCounterComponent
  },
  {
    path: 'supporting-document',
    component: UploadDocumentComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
