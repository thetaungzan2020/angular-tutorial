import {createAction, props} from "@ngrx/store";
import {CountState} from "../reducers/counter.reducer";

export const increment = createAction('[counter] increment');
export const decrement = createAction('[counter] decrement');
export const reset = createAction('[counter] reset');

export const addCountValue = createAction(
  '[addCount] custom count',
  props<{value:number}>()
)
