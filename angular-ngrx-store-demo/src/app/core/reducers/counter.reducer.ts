import {createReducer, on} from "@ngrx/store";
import {addCountValue, decrement, increment, reset} from "../actions/counter.action";
import {state} from "@angular/animations";

export interface CountState {
  count: number
}

export const initialCountState: CountState = {
  count: 0
}


export const counterReducer = createReducer(
  initialCountState,
  on(increment, (state) => ({...state, count: state.count + 1})),
  on(decrement, (state) => {
    return {
      ...state,
      count: state.count - 1
    }
  }),
  on(reset, state => ({...state, count: 0})),
  on(addCountValue, (state, action) => (

    {
      ...state,
      count: state.count + action.value
    }
  ))
)


