import {NgModule, isDevMode} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {counterReducer} from "./core/reducers/counter.reducer";
import { AddCounterComponent } from './components/add-counter/add-counter.component';
import {appReducer} from "./core/reducers/app.reducer";
import { HomeComponent } from './components/home/home.component';
import { UploadDocumentComponent } from './components/upload-document/upload-document.component';
import { NonpersonComponent } from './components/upload-document/nonperson/nonperson.component';

@NgModule({
  declarations: [
    AppComponent,
    AddCounterComponent,
    HomeComponent,
    UploadDocumentComponent,
    NonpersonComponent,
    /*LoginComponent*/
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(appReducer, {}),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: !isDevMode()}),
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
