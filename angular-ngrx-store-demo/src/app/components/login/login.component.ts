/*
import {Component, OnInit} from '@angular/core';
import {login} from "../../core/actions/login.action";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Store} from "@ngrx/store";
import {IAppState} from "../../core/reducers/app.reducer";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  constructor(private fb:FormBuilder,private store:Store<IAppState>) {
    this.loginForm = fb.group(
      {
        email: new FormControl('',[Validators.required,Validators.email]),
        password: new FormControl('',[Validators.required])
      }
    )
  }

  ngOnInit(): void {
  }


  login() {
    let getLoginForm = this.loginForm.value;
    console.log("form Value : ",getLoginForm);
    this.store.dispatch(login({auth:getLoginForm}));
  }
}
*/
